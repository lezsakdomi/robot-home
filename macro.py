#!/usr/bin/env python3

from ev3dev.ev3 import Motor, Button, Leds, Sound
from time import sleep
from sys import stdin, stdout, stderr
import shared
import math
from datetime import datetime
import argparse
from select import select

print("Processing command-line options...")
parser = argparse.ArgumentParser(description="Records and plays back movements")
parser.add_argument('-v', '--verbose', action='count', default=0,
			help="More verbosity")
parser.add_argument('-d', '--delay', type=float, default=0.1,
			metavar='SECONDS', help="Specifies sample rate")
parser.add_argument('-o', '--overlap', type=int, default=2,
			help="Use N samples just for error correction. More means smoother, less means more precision")
args=parser.parse_args()

print("Initialising interfaces...")
motors = [Motor(port) for port in ['outA', 'outB', 'outC', 'outD']]
btn = Button()

print("Defining functions...")
def clear():
	stderr.write("\033[2J")

def countdown(prompt, left=3, after="     "):
	i=left
	while i>0:
		print(prompt, i, after, end="\r")
		sleep(1)
		#reset_timer()
		#wait(1)
		i-=1
	Sound.beep().wait()

def wait(seconds=args.delay):
	global start_time, excepted_seconds
	excepted_seconds+=seconds
	
	while (datetime.now()-start_time).total_seconds()<excepted_seconds:
		#if select([stdin], [], [], 0.0):
		#	raise KeyboardInterrupt()
		pass

def visualcomp(a, b):
	if (a>b):
		return "\033[42m+\033[0m"
	elif (a<b):
		return "\033[41m-\033[0m"
	else:
		return "\033[43m0\033[0m"

def reset_timer():
	global start_time, excepted_seconds
	start_time=datetime.now()
	excepted_seconds=0

def present(device):
	return device and device.connected

def getpos(motor):
	if present(motor):
		return motor.position
	else:
		return False

def setpos(motor, pos):
	if present(motor) and pos:
		calculated=math.floor((pos-motor.position)/args.delay/args.overlap)
		motor.run_to_abs_pos(position_sp=pos, speed_sp=min(motor.max_speed, calculated))
		if args.verbose>=1:
			stderr.write(visualcomp(calculated, 0))

def reset():
	for motor in motors:
		if present(motor):
			motor.stop_action='coast'
			motor.reset()
			motor.stop_action='hold'

def release():
	for motor in motors:
		if present(motor):
			motor.stop_action='coast'
			motor.stop()
			motor.reset()

try:
	Leds.set_color(Leds.LEFT, Leds.GREEN)
	Leds.set_color(Leds.RIGHT, Leds.GREEN)
	print("Ready!")
	clear()
	input("Press enter to start recording")

	Leds.set_color(Leds.LEFT, Leds.AMBER)
	print("Recording initiated. Press again to finish")
	countdown("Recording in")
	clear()

	reset()
	print()
	print()
	print( "  /==================\\")
	print( "  |                  |" )
	print( "  | RECORDING ACTIVE |" )
	print( "  |                  |" )
	print("  \\==================/" )
	print()
	print("Press a button to end")
	record=[]
	Leds.set_color(Leds.LEFT, Leds.RED)
	try:
		reset_timer()
		while not btn.any():
			record.append([getpos(motor) for motor in motors])
			if (args.verbose>=2) and (len(record)>=2):
				for s in [visualcomp(z[0], z[1]) for z in zip(record[-2], record[-1])]:
					stderr.write(s)
				stderr.write("\n")
			wait()
	except KeyboardInterrupt:
		print("Assuming that you are using a terminal", file=stderr)
	Leds.set_color(Leds.LEFT, Leds.YELLOW)
	clear()
	print("Recording stopped.")
	print(len(record), "waypoints so far", file=stderr)

	input("Press enter to replay")
	while True:
		Leds.set_color(Leds.RIGHT, Leds.AMBER)
		countdown("", 5)
		clear()
		reset()
		print()
		print()
		print( "  /============\\")
		print( "  |            |" )
		print( "  |   REPLAY   |" )
		print( "  |            |" )
		print("  \\============/" )
		print()
		print("Press a button to cancel")
		Leds.set_color(Leds.RIGHT, Leds.RED)
		try:
			reset_timer()
			for status in record:
				if args.verbose>=0: print(status, file=stderr)
				for motor in motors:
					if present(motor):
						pass
						#if not motor.wait_while('running', 3*args.delay): print("\033[31mTimed out\033[0m", file=stderr)
				if btn.any():
					raise KeyboardInterrupt
				for pair in zip(motors, status):
					setpos(pair[0], pair[1])
				wait()
			clear()
			print("Finished")
		except KeyboardInterrupt:
			print("Cancelled by user")
		Leds.set_color(Leds.RIGHT, Leds.YELLOW)
		print("Replay ended. Do you wanna play it again?")
		input("Press enter for yes, quit somehow for no (Hint: For quit hold the back button in brickman, or press Ctrl+C in a terminal)")
except KeyboardInterrupt:
	Leds.all_off()
	release()
