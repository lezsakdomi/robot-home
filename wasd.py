#!/usr/bin/env python3
from ev3dev.ev3 import LargeMotor, MediumMotor
from curses import wrapper

def doerror(c):
	raise KeyboardInterrupt(c)

def set(left, right):
	global motors, speed
	motors['left'].run_forever(speed_sp=left*speed)
	motors['right'].run_forever(speed_sp=right*speed)

def incspeed(n):
	global speed
	speed+=n

def goto(pos, stop=False):
	global motors
	motors['action'].run_to_abs_pos(position_sp=pos, speed_sp=1000)
	motors['action'].wait_while('running')
	if stop:
		motors['action'].stop()

def action(dir):
	global motors
	motors['action'].run_forever(speed_sp=200*dir)

def hammer():
	global motors
	goto(150)
	goto(30, True)

def main(screen):
	global motors, speed
	
	motors = {
		'left': LargeMotor('outB'),
		'right': LargeMotor('outC'),
		'action': MediumMotor('outA')
	}
	speed = 1000
	for motor in motors.values():
		motor.reset()
	motors['action'].stop_action='brake'

	while True:
		c = chr(screen.getch())
		{
			'7': lambda c: set( 0, +1),
			'1': lambda c: set( 0, -1),
			'9': lambda c: set(+1,  0),
			'3': lambda c: set(-1,  0),
			'8': lambda c: set(+1, +1),
			'2': lambda c: set(-1, -1),
			'4': lambda c: set(-1, +1),
			'6': lambda c: set(+1, -1),
			'0': lambda c: set( 0,  0),
			'5': lambda c: hammer(),
			'+': lambda c: incspeed(+100),
			'-': lambda c: incspeed(-100),
			"\u0103": lambda c: action(+1),
			"\u015e": lambda c: action( 0),
			"\u0102": lambda c: action(-1),
			'w': lambda c: set(+1, +1),
			'a': lambda c: set(-1, +1),
			'd': lambda c: set(+1, -1),
			's': lambda c: set(-1, -1),
			' ': lambda c: hammer(),
			'q': lambda c: set( 0, +1),
			'e': lambda c: set(+1,  0),
			"\n": lambda c: set(0, 0),
			"\u019a": lambda c: True
		}.get(c, doerror)(c)

if __name__=="__main__":
	try:
		wrapper(main)
	except Exception as e:
		goto(0)
		set(0, 0)
		raise e
