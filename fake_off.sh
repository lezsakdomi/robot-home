#!/bin/bash
echo This program is intended for simulating empty accumulators
echo Note that this sleep is fake: The batteries are still draining. Do not forget me in this state for while!
echo 
echo To exit, long press the back button
echo
for i in {3..1}; do
	echo -ne "Going down for sleep in $i \r"
	sleep 1
done
echo -ne "\033[3J"

for led in /sys/class/leds/*; do
	echo 0 >$led/brightness
done

for port in /sys/class/lego-port/*; do
	echo other-uart >$port/mode 2>/dev/null
done

wakeup(){
	for led in /sys/class/leds/*green*; do
		echo 255 >$led/brightness
	done
	
	for port in /sys/class/lego-port/*; do
		echo auto >$port/mode
	done
	
	exit
}
trap wakeup SIGINT SIGTERM

while true; do
	echo -ne "\033[H"
	cat /dev/zero >/dev/fb0 2>/dev/null
	sleep 1
done
