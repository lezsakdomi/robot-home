#!/bin/false
from ev3dev.ev3 import Motor, LargeMotor, MediumMotor, ColorSensor, GyroSensor
import math

class Wheel:
	motor=None
	circumference=None
	radius=None
	diameter=None
	def __init__(self, motor=None, circumference=None, radius=None, diameter=None):
		self.motor=motor
		if circumference:
			self.circumference=circumference
			self.radius=circumference/math.pi
			self.diameter=circumference/math.pi/2
		elif radius:
			self.circumference=2*radius*math.pi
			self.radius=radius
			self.diameter=2*radius
		elif diameter:
			self.circumference=diameter*math.pi
			self.radius=diameter/2
			self.diameter=diameter

class Robot:
	"""Our robot construction"""
	motors={
		'left': Wheel(LargeMotor('outA'), circumference=30),
		'right': Wheel(LargeMotor('outD'), circumference=30),
		'steering': None
	}
	wheelbase=72
	sensors={
		'light': ColorSensor('in2'),
		'gyro': GyroSensor('in3'),
		'color': ColorSensor('in1')
	}

robot=Robot()
